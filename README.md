# Triggered pipelines

Triggered pipelines will need artifacts of **jobs** in the upstream
**pipeline**. Each job creates its own pipelines.

**NOTE**: the current mechanism can only access parent pipeline artifacts
from public projects.

```yaml
  script:
    - !reference [.def-get-artifacts]
    - |
      # get_artifacts "testjob" output
      get_artifacts --upstream-job-name ${CI_JOB_NAME} \ # required, use $CI_JOB_NAME if same name as current job
                    --upstream-project-name "A name" \ # optional
                    --upstream-project-id "project ID" \ # optional
                    --upstream-pipeline-id "pipeline ID" \ # optional
                    --output <FOLDER> \ # optional
```
The first parameter is the name of the upstream job.
The second parameter is the name of the folder where the artifacts are being put.
